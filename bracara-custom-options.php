<?php
/*
Plugin Name: BracaraTech Custom Options Plugin
Plugin URI: http://www.spinpark.pt/empresas.info.php?lang=pt&idEmpresa=58
Description: This is a plugin to configure custom options like Theme Options, etc.
Author: Bracaratech
Version: 1.0
* Author URI: http://www.bracaratech.com/
* License: GPLv2 or later
*/

define('WP_DEBUG', true);

$path = __DIR__ . "/../../..";

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';


/**
 * @return array
 */
function magicFieldsRequired() {
	/**
	 * Add in these return the magic field & groups application needs to be installed when BracaraTech Custom Options Plugin is activated.
	 */
	return [
		"post_type" => "btech_theme_options",
		"groups" => [
			"memcached" => [
				"group_id" => NULL,
				"name" => "memcached",
				"label" => "Memcached",
				"duplicated" => 0,
				"expanded" => 1
			],
			"logs" => [
				"group_id" => NULL,
				"name" => "logs",
				"label" => "Logs",
				"duplicated" => 0,
				"expanded" => 1
			],
			"subscription_api" => [
				"group_id" => NULL,
				"name" => "subscription_api",
				"label" => "Subscription",
				"duplicated" => 0,
				"expanded" => 1
			],
			"app" => [
				"group_id" => NULL,
				"name" => "app",
				"label" => "Application",
				"duplicated" => 0,
				"expanded" => 1
			],
			"seo" => [
				"group_id" => NULL,
				"name" => "seo",
				"label" => "SEO",
				"duplicated" => 0,
				"expanded" => 1
			],
		],
		"magicFields" => [
			"memcached" => [
				[
					"name" => "memcached_host",
					"label" => "Host",
					"description" => "IP from memcached server",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 4,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:\"evalueate\";s:1:\"1\";s:4:\"size\";s:2:\"25\";}',
				],
				[
					"name" => "memcached_port",
					"label" => "Port",
					"description" => "Port from memcached server",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 5,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "memcached_active",
					"label" => "Active",
					"description" => "Check bellow box to Activate Memcached.",
					"type" => "checkbox",
					"required_field" => 0,
					"display_order" => 0,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:0:{}',
				],
				[
					"name" => "memcached_default_cache_hash",
					"label" => "Default Cache Hash",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 3,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "memcached_active_tags",
					"label" => "Active Tags",
					"description" => "Check bellow box to Tags in Memcached.",
					"type" => "checkbox",
					"required_field" => 0,
					"display_order" => 1,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:0:{}',
				],
				[
					"name" => "memcached_default_expiration_time",
					"label" => "Default Expiration Time",
					"description" => "Time In Hours",
					"type" => "slider",
					"required_field" => 0,
					"display_order" => 2,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:3:{s:9:"value_min";s:1:"0";s:9:"value_max";s:2:"48";s:8:"stepping";s:1:"1";}',
				]
			],
			"logs" => [
				[	
					"name" => "logs_folder_path",
					"label" => "Folder Path",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 2,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "logs_active",
					"label" => "Active",
					"description" => "",
					"type" => "checkbox",
					"required_field" => 0,
					"display_order" => 0,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:0:{}',
				],
				[
					"name" => "logs_filename",
					"label" => "Filename",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 0,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "logs_one_log_per_day",
					"label" => "One Log per day",
					"description" => "",
					"type" => "checkbox",
					"required_field" => 0,
					"display_order" => 0,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:0:{}',
				],
				[
					"name" => "logs_close_file_per_log",
					"label" => "Close File Per Log",
					"description" => "",
					"type" => "checkbox",
					"required_field" => 0,
					"display_order" => 0,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:0:{}',
				],
			],
			"subscription_api" => [
				[
					"name" => "subscription_api_partner_id",
					"label" => "Partner ID",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 1,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "subscription_api_service_id",
					"label" => "Service ID",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 2,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "subscription_api_operator_id",
					"label" => "Operator ID",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 3,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "subscription_api_msisdn_prefix",
					"label" => "MSISDN Prefix",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 5,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "subscription_api_msisdn_length",
					"label" => "MSISDN Length",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 6,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "subscription_api_need_token",
					"label" => "Need Token",
					"description" => "",
					"type" => "checkbox",
					"required_field" => 0,
					"display_order" => 0,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:0:{}',
				],
				[
					"name" => "subscription_api_password",
					"label" => "Password",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 4,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
			],
			"app" => [
				[
					"name" => "app_name",
					"label" => "Name",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 0,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"25";}',
				],
				[
					"name" => "app_title",
					"label" => "Title",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 1,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"50";}',
				],
				[
					"name" => "app_copyright_text",
					"label" => "CopyRight Text",
					"description" => "Type somewhere \"(Y)\" to actual year be displayed",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 2,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"100";}',
				],
				[
					"name" => "app_social_youtube",
					"label" => "Youtube URL",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 3,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"200";}',
				],
				[
					"name" => "app_social_google_plus",
					"label" => "Google Plus URL",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 4,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"200";}',
				],
				[
					"name" => "app_social_facebook",
					"label" => "Facebook URL",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 5,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"200";}',
				],
				[
					"name" => "app_social_twitter",
					"label" => "Twitter URL",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 6,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"200";}',
				],
				[
					"name" => "app_social_linkedin",
					"label" => "LinkedIn URL",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 7,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"200";}',
				],
				[
					"name" => "app_social_instagram",
					"label" => "Instagram URL",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 8,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"200";}',
				],
				[
					"name" => "app_google_analytics_embed_code",
					"label" => "Google Analytics Embed Code",
					"description" => "",
					"type" => "markdown_editor",
					"required_field" => 0,
					"display_order" => 9,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:0:{}',
				],
				[
					"name" => "app_terms_and_conditions",
					"label" => "Terms and Conditions Text HTML",
					"description" => "Format: HTML",
					"type" => "multiline",
					"required_field" => 0,
					"display_order" => 10,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:3:{s:6:"height";s:2:"10";s:5:"width";s:2:"23";s:11:"hide_visual";s:1:"0";}',
				],
				[
					"name" => "app_logo_image",
					"label" => "Logo Image",
					"description" => "",
					"type" => "image",
					"required_field" => 0,
					"display_order" => 10,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:4:{s:9:"css_class";s:12:"magic_fields";s:10:"max_height";s:0:"";s:9:"max_width";s:0:"";s:6:"custom";s:0:"";}',
				],
			],
			"seo" => [
				[
					"name" => "seo_meta_description",
					"label" => "Meta Description",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 0,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"200";}',
				],
				[
					"name" => "seo_meta_author",
					"label" => "Meta Author",
					"description" => "",
					"type" => "textbox",
					"required_field" => 0,
					"display_order" => 1,
					"duplicated" => 0,
					"active" => 1,
					"options" => 'a:2:{s:9:"evalueate";s:1:"1";s:4:"size";s:2:"200";}',
				],
			]
		]
	];
}


// Register Custom Post Type
function bracara_create_theme_options() {

	$labels = array(
		'name'                  => _x( 'Theme Options', 'Theme Option'),
		'singular_name'         => _x( 'Theme Option', 'Theme Option'),
		'menu_name'             => __( 'Theme Options'),
		'name_admin_bar'        => __( 'Theme Option'),
		'add_new_item'          => __( 'New Theme ', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' )
	);
	$args = array(
		'label'                 => __( 'Post Type', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array("title"),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 100,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'btech_theme_options', $args );
}

/**
 * @param array $group
 * @param String $post_type
 * @return bool|int
 */
function createGroup($group = NULL, $post_type = "btech_theme_options") {
	if(!is_array($group)) {
		return false;
	}
	else {
		global $wpdb;
		$name = $group["name"];
		$label = $group["label"];
		$duplicated = $group["duplicated"];
		$expanded = $group["expanded"];
		$insertQuery = "INSERT INTO wp_mf_custom_groups 
		(`name`, label, post_type, duplicated, expanded) 
		VALUES
		('$name', '$label', '$post_type', $duplicated, $expanded)";
		$wpdb->query($insertQuery);
		return $wpdb->insert_id;
	}
}

/**
 * @param null $magicField
 * @param null $group_id
 * @param string $post_type
 * @return bool|int
 */
function createMagicField($magicField = NULL, $group_id = NULL, $post_type = "btech_theme_options") {
	if(!is_array($magicField) || is_null($group_id)) {
		return false;
	}
	else {
		global $wpdb;
		$name = $magicField["name"];
		$label = $magicField["label"];
		$description = $magicField["description"];
		$type = $magicField["type"];
		$required_field = $magicField["required_field"];
		$display_order = $magicField["display_order"];
		$duplicated = $magicField["duplicated"];
		$active = $magicField["active"];
		$options = $magicField["options"];

		$insertQuery = "INSERT INTO wp_mf_custom_fields 
		(`name`, label, description, post_type, custom_group_id, `type`, required_field, display_order, duplicated, active, options) 
	  	VALUES
		('$name', '$label', '$description', '$post_type', $group_id, '$type', $required_field, $display_order, $duplicated, $active, '$options')";
		$wpdb->query($insertQuery);
		return $wpdb->insert_id;
	}
}

/**
 * @param String $group_name
 * @param string $post_type
 * @return bool|array
 */

function checkGroupExists($group_name, $post_type = "btech_theme_options") {
	global $wpdb;
	$query = "SELECT * FROM wp_mf_custom_groups WHERE name LIKE '$group_name' and post_type LIKE '$post_type'";
	$results = $wpdb->get_results($query);
	if(count($results) > 0)
		return $results[0];
	else
		return false;
}

function checkMagicFieldExists($magicFieldName, $post_type = "btech_theme_options") {
	global $wpdb;
	$query = "select * from wp_mf_custom_fields where name LIKE '$magicFieldName' and post_type LIKE '$post_type'";
	$results = $wpdb->get_results($query);
	if(count($results) > 0)
		return $results[0];
	else
		return false;
}

/**
 *
 */
function my_plugin_activate() {
	global $wpdb;
	$magicFiedsRequired = magicFieldsRequired();

	if(is_array($magicFiedsRequired) && isset($magicFiedsRequired["groups"])) {
		/**
		 * Phase 1: Check whether groups exists, if not create them.
		 */
		foreach ($magicFiedsRequired["groups"] as $key => $value) {
			$result = NULL;
			$result = checkGroupExists($key);
			if(is_object($result)) {
				$magicFiedsRequired["groups"][$key]["group_id"] = $result->id;
			}
			else {
				$id = createGroup($magicFiedsRequired["groups"][$key]);
				$magicFiedsRequired["groups"][$key]["group_id"] = $id;
			}
		}

		/**
		 * Phase 2: Check whether magic fields exists, if not create them.
		 */
		foreach ($magicFiedsRequired["magicFields"] as $key => $value) {
			foreach($magicFiedsRequired["magicFields"][$key] as $magicField){
				$result = NULL;
				$result = checkMagicFieldExists($magicField["name"]);
				if(!is_object($result)) {
					$group_id = $magicFiedsRequired["groups"][$key]["group_id"];
					createMagicField($magicField, $group_id);
				}
			}
		}
		return true;
	}
	else {
		return false;
	}
}

register_activation_hook( __FILE__, 'my_plugin_activate' );
add_action( 'init', 'bracara_create_theme_options', 0 );

